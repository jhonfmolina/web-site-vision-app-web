import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "bootstrap";
import "./assets/app.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faUserSecret,
  faEdit,
  faBaby,
  faBath,
  faAmbulance,
  faEnvelope,
  faMapMarkerAlt,
  faPhone,
  faAngleDoubleRight,
  faQuoteLeft,
  faQuoteRight,
  faHandHoldingUsd,
  faTh,
  faThumbsUp,
  faChartPie,
  faLaptopCode,
  faChalkboardTeacher,
  faLayerGroup
} from "@fortawesome/free-solid-svg-icons";
import { faAddressCard } from "@fortawesome/free-solid-svg-icons/faAddressCard";
import { faSpinner } from "@fortawesome/free-solid-svg-icons/faSpinner";
import {
  FontAwesomeIcon,
  FontAwesomeLayers,
  FontAwesomeLayersText,
} from "@fortawesome/vue-fontawesome";

library.add(
  faUserSecret,
  faAddressCard,
  faSpinner,
  faEdit,
  faBaby,
  faBath,
  faAmbulance,
  faEnvelope,
  faMapMarkerAlt,
  faPhone,
  faAngleDoubleRight,
  faQuoteLeft,
  faQuoteRight,
  faHandHoldingUsd,
  faTh,
  faThumbsUp,
  faChartPie,
  faLaptopCode,
  faChalkboardTeacher,
  faLayerGroup
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("font-awesome-layers", FontAwesomeLayers);
Vue.component("font-awesome-layers-text", FontAwesomeLayersText);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
